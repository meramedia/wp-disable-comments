<?php

namespace WP_Disable_Comments;


class DisableComments
{

    function __construct()
    {
        add_action('after_setup_theme', [$this, 'update_options_comments_and_pings']);

        // Disable support for comments and trackbacks in post types
        add_action('admin_init', [$this, 'disable_comments_post_types_support']);

        // Close comments on the front-end
        add_filter('comments_open', [$this, 'disable_comments_status'], 20, 2);
        add_filter('pings_open', [$this, 'disable_comments_status'], 20, 2);

        // Hide existing comments
        add_filter('comments_array', [$this, 'disable_comments_hide_existing_comments'], 10, 2);

        // Remove comments page in menu
        add_action('admin_menu', [$this, 'disable_comments_admin_menu']);
        // Redirect any user trying to access comments page
        add_action('admin_init', [$this, 'disable_comments_admin_menu_redirect']);

        // Remove comments metabox from dashboard
        add_action('admin_init', [$this, 'disable_comments_dashboard']);

        // Remove comments links from admin bar
        add_action('init', [$this, 'disable_comments_admin_bar']);

        // Remove Comments from admin-bar
        add_action('admin_bar_menu', [$this, 'remove_wp_nodes'], 999);
    }

    /**
     * Remove Comments from the admin-bar
     */
    public function remove_wp_nodes()
    {
        /**
         * @var $wp_admin_bar \WP_Admin_Bar
         */
        global $wp_admin_bar;
        $wp_admin_bar->remove_node('comments');
    }

    /**
     * Set comments & pings options to false
     */
    public function update_options_comments_and_pings()
    {

        // Turn off comments
        if ('' != get_option('default_ping_status')) {
            update_option('default_ping_status', '');
        } // end if

        // Turn off pings
        if ('' != get_option('default_comment_status')) {
            update_option('default_comment_status', '');
        } // end if

    }


    /**
     * Disable support for comments and trackbacks in post types
     */
    public function disable_comments_post_types_support()
    {
        $post_types = get_post_types();
        foreach ($post_types as $post_type) {
            if (post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }

    /**
     * Close comments on the front-end
     *
     * @return bool
     */
    public function disable_comments_status()
    {
        return false;
    }


    /**
     * Hide existing comments
     *
     * @param $comments
     *
     * @return array
     */
    public function disable_comments_hide_existing_comments($comments)
    {
        $comments = array();

        return $comments;
    }

    /**
     * Remove comments page in menu
     */
    public function disable_comments_admin_menu()
    {
        remove_menu_page('edit-comments.php');
    }


    /**
     * Redirect any user trying to access comments page
     */
    public function disable_comments_admin_menu_redirect()
    {
        global $pagenow;
        if ($pagenow === 'edit-comments.php') {
            wp_redirect(admin_url());
            exit;
        }
    }


    /**
     * Remove comments metabox from dashboard
     */
    public function disable_comments_dashboard()
    {
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    }


    /**
     * Remove comments links from admin bar
     */
    public function disable_comments_admin_bar()
    {
        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
    }
}
