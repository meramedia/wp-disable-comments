<?php
/**
 * Plugin Name: Disable comments
 * Description: This plugin removes comment-functionality
 * Version: 1.0.0
 * Author: Meramedia
 * Author URI: https://meramedia.se
 */

namespace WP_Disable_Comments;

define( 'DISABLE_COMMENTS_ROOT', __DIR__ . DIRECTORY_SEPARATOR );
define( 'DISABLE_COMMENTS_SRC', DISABLE_COMMENTS_ROOT . 'app' . DIRECTORY_SEPARATOR );

require_once( DISABLE_COMMENTS_SRC . 'DisableComments.php');

// Init the plugin
new DisableComments();
